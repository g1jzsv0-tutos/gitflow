# Histoire de ce projet

```
branches
    main → 1 commit par livraison en production
    develop → 1 commit par fonctionnalité à tester
    Toute opération de codage correspond à 1 branche
        fix-xxxxxxxxxx → correction d'un bug
        feature-xxxxxxxxxx ou feat-xxxxxxxxxx → développement d'une fonctionnalité
        wip-xxxxxxxxxx → work in progress (je veux faire des trucs, je sais pas trop ce que ça va donner)
    Créer une branche
        git checkout -b <nom de la branche>
        git push --set-upstream origin <nom de la branche>

Exemple de projet
Créer le projet (sous gitlab exemple)
    New project
    Puis sous git bash
        git clone <adresse du projet>
    Fermer git bash
    Aller dans le répertoire du projet
    Ouvrir git bash dans ce répertoire
Créer la branche develop
    Dans git bash
        Créer la branche localement
            git checkout -b develop
        Pusher la branche vers le dépôt distant (github)
            git push --set-upstream origin <nom de la branche>
Imaginons qu'on a deux travailleu·r·se·s
Gugusse 1 travaille sur la fonctionnalité 1
    Commencer à faire des trucs
        Créer la fonctionnalité 1
            Créer la branche
                git checkout -b feature-fonctionnalite-1
            Faire la fonctionnalité (ajouter des fichiers, éditer…)
            Ajouter les fichiers modifiés à la liste des fichiers à « suivre »
                git add .
            Valider (commiter)
                git commit -m "[WIP] login du client : vérification de l'email"
            Pusher la branche
                git push --set-upstream origin feature-fonctionnalite-1
    Continuer à faire des trucs
        Enrichir la fonctionnalité
            Éditer le fichier fonctionnalité.txt en rajoutant plein de trucs
        Commiter et pusher
            git add . && git commit -m "Finalisation de la fonctionnalité 1" && git push
Gugusse 2 travaille sur une autre fonctionnalité (ce fichier)
    Initier l'histoire du projet
        Créer la branche feature-histoire-du-projet
        Y créer ce fichier :-)
        git add . && git commit -m "[WIP] Explication du gitflow" && git push --set-upstream origin feature-histoire-du-projet
    Continuer l'histoire du projet
        (écrire des trucs ici)
    Commiter et pusher
        git add . && git commit -m "Explication du gitflow OK" && git push
Ajouter les deux fonctionnalités (fonctionnalité 1 ET histoire du projet) à develop
    Ajouter fonctionnalité 1
        Aller sur develop
            git checkout develop
        Faire un merge de feature-fonctionnalite-1 (en concaténant tous les commits)
            git merge --squash feature-fonctionnalite-1
        Commiter en local
            git commit -m "Ajout de la fonctionnalité 1"
        Envoyer tout au big boss (le dépôt distant)
            git push --force-with-lease
    Ajouter histoire du projet
        (Aller sur develop
            git checkout develop)
        Faire un merge de feature-histoire-du-projet (en concaténant tous les commits)
            git merge --squash feature-histoire-du-projet
        Commiter en local
            git commit -m "Ajout de l'histoire du projet"
        Envoyer tout au big boss (le dépôt distant)
            git push --force-with-lease
Faire la version qui correspond à la livraison en production
    Aller sur main
        git checkout main
    Ajouter les modifications de develop
        git merge --squash develop
    Commiter avec un beau message qui résume bien
        git commit -m "Fonctionnalités ajoutés:\n1. Fonctionnalité 1.\n2. Histoire du projet"
    Envoyer tout au big boss
        git push --force-with-lease
Supprimer, le cas échéant, les branches inutiles
    Supprimer la branche en local
        git branch -d feature-fonctionnalite-1
        (idem pour feature-histoire-du-projet)
    Supprimer la branche distante
        git push origin --delete feature-fonctionnalite-1
        (idem pour feature-histoire-du-projet)
```